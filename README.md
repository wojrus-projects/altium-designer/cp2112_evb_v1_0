# Specyfikacja

Urządzenie jest uniwersalnym interfejsem I2C-master/SMBus podłączanym do PC przez USB 2.0.
- Układ Silicon Labs CP2112 (https://www.silabs.com/interface/usb-bridges/classic/device.cp2112)
  - USB 2.0 Full-Speed (12 Mbps)
  - Klasa HID (nie wymaga dedykowanego sterownika)
  - Obsługa uśpienia (suspend)
  - Zasilanie z hosta USB
- Interfejs I2C-master
  - f_scl=250...400'000 Hz
  - Adresowanie 7-bitowe
  - Maksymalna długość danych I2C:
    - Read: 512 bajtów
    - Write: 61 bajtów
  - Automatyczna retransmisja adresu gdy NACK
  - Translacja poziomów napięciowych na magistrali I2C za pomocą bufora PCA9517ADP
  - Trzy sposoby ustalenia poziomów napięciowych wybierane zworkami:
    - Zasilanie bufora bezpośrednio z USB (5.0 V odjąć spadek na diodzie Schottky D4)
    - Zasilanie bufora z USB przez regulowany stabilizator napięcia LDO (wybierane: 1.8, 2.5, 3.0, 3.3, 3.7 V lub kombinacje)
    - Zasilanie bufora z zewnątrz modułu, przez złącze śrubowe lub szpilkowe (zakres 0.9-5.5 V odjąć spadek na diodzie Schottky D3)
- 8 uniwersalnych portów GPIO
  - Konfigurowalnye jako wejścia lub wyjścia open-drain / push-pull
  - Poziomy CMOS 3.3 V
- 2 LED-y RX/TX
- LED sygnalizująca stan uśpienia USB
- Złącze śrubowe (terminal block): SCL, SDA, GND, V_I2C_BUFFER
- Opcjonalne dwa złącza szpilkowe (headers) 9-stykowe do płytek/przewodów stykowych
  - Raster 2,54 mm
  - Rozsunięte na 39,0 mm (pasuje do [Wish Board WB-104-1](https://www.tme.eu/pl/details/wb-104-1/plytki-uniwersalne/wisher-enterprise/))
- Gniazdo USB typu Micro-B

# Projekt PCB

Schemat: [doc/CP2112_EVB_V1_0_SCH.pdf](doc/CP2112_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/CP2112_EVB_V1_0_3D.pdf](doc/CP2112_EVB_V1_0_3D.pdf)

| Top layer | Top assembly | 3D preview |
|-----------|--------------|------------|
| ![](doc/foto/PCB-altium-top.png "Top layer")  | ![](doc/foto/PCB-altium-assembly.png "Top assembly") | ![](doc/foto/PCB-altium-3D.png "3D preview") |

# Wyprodukowane PCB

| PCB top bez elementów |  PCB zmontowana |
|-----------------------|-----------------|
| ![](doc/foto/PCB-bare.jpg "PCB bare")  | ![](doc/foto/PCB-assembled.jpg "PCB assembled") |

# Oprogramowanie

Układ CP2112 może być oprogramowany na dwa sposoby:
- Niskopoziomowo z użyciem pakietów USB-HID.
  - AN495: https://www.silabs.com/documents/public/application-notes/an495-cp2112-interface-specification.pdf
  - AN532: https://www.silabs.com/documents/public/application-notes/AN532.pdf
- Wysokopoziomowo z użyciem biblioteki dostarczonej przez producenta.
  - AN496: https://www.silabs.com/documents/public/application-notes/an496-hid-usb-to-smbus-api-specification.pdf

Wszystkie potrzebne biblioteki znajdują się w SDK: https://www.silabs.com/documents/public/software/USBXpressHostSDK-Win.zip

## Program I2CTool

Moduł współpracuje z darmowym programem I2CTool:
- [Repozytorium](https://gitlab.com/wojrus-projects/visual-studio/i2ctool)
- [Pliki wykonywalne](https://gitlab.com/wojrus-projects/visual-studio/i2ctool/-/tree/main/release)

Program *I2CTool* jest uniwersalnym narzędziem CLI do niskopoziomowych testów i diagnostyki urządzeń I2C slave. Funkcjonalność jest podobna do pakietu *i2c-tools* w Linuksie. Podstawowe funkcje/rozkazy:

- **detect** - wyszukanie adresów slave podłączonych do magistrali I2C.
- **wr (write)** - zapisanie od 1 do N bajtów do slave.
- **rd (read)** - odczytanie od 1 do N bajtów ze slave.
- **rda (read with address)** - odczytanie od 1 do N bajtów ze slave z możliwością podania adresu początkowego pamięci np. EEPROM.
- **dev (device)** - wysokopoziomowa obsługa konkretnych urządzeń slave.

Platforma: Windows 10 64-bit 22H2

Toolchain: Visual Studio Community 2019 v16.11.22

Język: C++ 2020 latest

Instalacja: nie jest wymagana. Interfejs CP2112 nie wymaga sterowników (używa protokół USB-HID).

# Licencja

MIT
